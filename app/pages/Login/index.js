import React, { Component } from 'react';
import { Link } from 'react-router';
import { RaisedButton, AppBar, Paper, TextField, ClearFix } from 'material-ui';
require('./style.scss');

export default class Home extends Component {

  render () {
    return (
      <div className="Login-screen">
        <Paper zDepth={2} rounded={false} className="Login-screen__Paper">
          <AppBar
            title="Log In"
            zDepth={0}
            showMenuIconButton={false} />
          <ClearFix style={{margin : '0 20px'}}>
            <div>
              <TextField
                hintText="type your username"
                floatingLabelText="Login" />
            </div>
            <div>
              <TextField
                hintText="type your password"
                floatingLabelText="Password"
                type="password" />
            </div>
            <div>
              <Link to="home" className="Login-screen__Button">
                <RaisedButton
                  label="Login"
                  primary={true}
                  style={{ margin : '40px auto 0', width : '100%' }} />
              </Link>
            </div>
          </ClearFix>
        </Paper>
      </div>
    );
  }

}
