import React, { Component } from 'react';
import Finder from '../../components/Finder';
import { Grid, Row, Column } from '../../components/Layout';
import { List, ListItem, Paper } from 'material-ui';

const currentDate = new Date();

const videosInformation = {
  time     : currentDate.getHours() + ':' + currentDate.getMinutes(),
  date     : (currentDate.getMonth() + 1) + '/' + currentDate.getDate() + '/' + currentDate.getFullYear(),
  gameId   : Math.floor((Math.random() * 50000)),
  username : 'Robert Johnson',
  gameName : 'Black Jack',
};

export default class PlayersInfo extends Component {

  render () {
    return (
      <Grid>
        <Row>
          <Column><Finder /></Column>
        </Row>
        <Row>
          <Column>
            <Paper>
              <List subheader="Video Info">
                <ListItem primaryText={`Username: ${videosInformation.username}`} />
                <ListItem primaryText={`Time: ${videosInformation.time}`} />
                <ListItem primaryText={`Date: ${videosInformation.date}`} />
                <ListItem primaryText={`Game ID: ${videosInformation.gameId}`} />
                <ListItem primaryText={`Game Name: ${videosInformation.gameName}`} />
              </List>
            </Paper>
          </Column>
          <Column>
            <iframe src="http://abcnews.go.com/video/embed?id=19923493" width="640" height="360" scrolling="no">
            </iframe>
          </Column>
        </Row>
      </Grid>
    );
  }
}
