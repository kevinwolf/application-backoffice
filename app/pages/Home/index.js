import React, { Component } from 'react';
import Finder from '../../components/Finder';
import { Grid, Row, Column } from '../../components/Layout';
import { Table } from 'material-ui';

const cols = {
  id         : { content : 'ID', tooltip : 'The ID' },
  dateTime   : { content : 'Date/Time', tooltip : 'Date and time' },
  game       : { content : 'Game', tooltip : 'Game'},
  dealerName : { content : 'Dealer Name', tooltip : 'Name of dealer' },
  playerNick : { content : 'Player Nickname', tooltip : 'Player Nickname' },
  betAmount  : { content : 'Bet Amount', tooltip : 'Bet Amount' },
  cardRead   : { content : 'Card Read', tooltip : 'Card Read' },
};

const colOrder = [ 'id', 'dateTime', 'game', 'dealerName', 'playerNick', 'betAmount', 'cardRead' ];

const rows = [{
  id         : { content : '12648' },
  dateTime   : { content : '07/27/15 22:23' },
  game       : { content : 'Black Jack' },
  dealerName : { content : 'Rose' },
  playerNick : { content : 'CrazyBettor' },
  betAmount  : { content : '$75' },
  cardRead   : { content : 'T &spades;' },
}];


export default class Home extends Component {

  constructor (props) {
    super(props);
    this.state = { rowData : rows };
  }

  render () {
    return (
      <Grid>
        <Row>
          <Column><Finder /></Column>
        </Row>

        <Row>
          <Column>
            <Table
              headerColumns={cols}
              columnOrder={colOrder}
              rowData={this.state.rowData}
              displaySelectAll={false}
              displayRowCheckbox={false} />
          </Column>
        </Row>
      </Grid>
    );
  }

}
