import React, { Component } from 'react';
import Finder from '../../components/Finder';
import { Grid, Row, Column } from '../../components/Layout';
import { List, ListItem, ListDivider, Paper, Checkbox, TextField } from 'material-ui';

const playersInformation = {
  age      : 25,
  country  : 'Costa Rica',
  currency : 'Colones',
  language : 'spanish',
  license  : 'Provider',
  name     : 'Marcelo',
  nickName : 'Maketroli',
};

export default class PlayersInfo extends Component {

  render () {
    return (
      <Grid>
        <Row>
          <Column><Finder /></Column>
        </Row>
        <Row>
          <Column>
            <Paper>
              <List subheader="Player Info">
                <ListItem primaryText={`Name: ${playersInformation.name}`} />
                <ListItem primaryText={`Nickname: ${playersInformation.nickName}`} />
                <ListItem primaryText={`Age: ${playersInformation.age}`} />
                <ListItem primaryText={`Language: ${playersInformation.language}`} />
                <ListItem primaryText={`Country: ${playersInformation.country}`} />
                <ListItem primaryText={`Currency: ${playersInformation.currency}`} />
                <ListItem primaryText={`License: ${playersInformation.license}`} />
              </List>
            </Paper>
          </Column>

          <Column>
            <Paper>
              <List subheader="Blocks">
                <ListItem leftCheckbox={<Checkbox />} primaryText="Block Chat" />
                <ListItem leftCheckbox={<Checkbox />} primaryText="Block from playing" />
                <ListItem leftCheckbox={<Checkbox />} primaryText="Block from site" />
                <ListDivider />
                <ListItem>
                  <TextField floatingLabelText="Notes" multiLine={true} fullWidth={true} />
                </ListItem>
              </List>
            </Paper>
          </Column>

          <Column>
            <Paper>
              <List subheader="Cash balance">
                <ListItem primaryText="Earnings" />
                <ListItem primaryText="Loss" />
                <ListItem primaryText="Earnings, Loss" />
                <ListItem primaryText="Money Withdraw" />
                <ListItem primaryText="Earnings, Loss" />
                <ListItem primaryText="Earnings, Loss" />
                <ListItem primaryText="Earnings, Loss" />
              </List>
            </Paper>
          </Column>

          <Column>
            <Paper>
              <List subheader="Hands history">
                <ListItem primaryText="Hands history 1" />
                <ListItem primaryText="Hands history 2" />
                <ListItem primaryText="Hands history 3" />
                <ListItem primaryText="Hands history 4" />
                <ListItem primaryText="Hands history 5" />
                <ListItem primaryText="Hands history 6" />
                <ListItem primaryText="Hands history 7" />
              </List>
            </Paper>
          </Column>
        </Row>
      </Grid>
    );
  }
}
