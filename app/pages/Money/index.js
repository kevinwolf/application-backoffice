import React, { Component } from 'react';
import { Grid, Row, Column } from '../../components/Layout';
import { Table } from 'material-ui';

const cols = {
  amount : { content : 'Amount' },
  time   : { content : 'Time' },
  date   : { content : 'Date' },
  reason : { content : 'Reason' },
};

const colOrder = [ 'amount', 'time', 'date', 'reason' ];

const rowsIncome = [{
  amount : { content : '$895.33' },
  time   : { content : '22:23 EST' },
  date   : { content : '07/27/2015' },
  reason : { content : 'Reason for income' },
},
{
  amount : { content : '$900.33' },
  time   : { content : '21:43 EST' },
  date   : { content : '07/29/2015' },
  reason : { content : 'Reason for income' },
}];

const rowsOutcome = [{
  amount : { content : '$867.66' },
  time   : { content : '22:23 EST' },
  date   : { content : '07/24/2015' },
  reason : { content : 'Reason for Outcome' },
},
{
  amount : { content : '$798.43' },
  time   : { content : '21:43 EST' },
  date   : { content : '07/25/2015' },
  reason : { content : 'Reason for Outcome' },
}];

export default class Money extends Component {

  static contextTypes = {
    router : React.PropTypes.func,
  }

  constructor (props) {
    super(props);
    this.state = { rowsIncome : rowsIncome, rowsOutcome : rowsOutcome };
  }

  render () {
    return (
      <Grid>
        <Row>
          <Column><h4>Money Income</h4></Column>
        </Row>

        <Row>
          <Column>
            <Table
              headerColumns={cols}
              columnOrder={colOrder}
              rowData={this.state.rowsIncome}
              displaySelectAll={false}
              displayRowCheckbox={false} />
            </Column>
        </Row>

        <Row>
          <Column><h4>Money Outcome</h4></Column>
        </Row>

        <Row>
          <Column>
            <Table
              headerColumns={cols}
              columnOrder={colOrder}
              rowData={this.state.rowsOutcome}
              displaySelectAll={false}
              displayRowCheckbox={false} />
          </Column>
        </Row>
      </Grid>
    );
  }

}
