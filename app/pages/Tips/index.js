import React, { Component } from 'react';
import Finder from '../../components/Finder';
import { Grid, Row, Column } from '../../components/Layout';
import { Table } from 'material-ui';

const cols = {
  dealer : { content : 'Dealer', tooltip : 'The Dealer that has received the tip' },
  player : { content : 'Player', tooltip : 'The Player that has given the tip' },
  amount : { content : 'Tip amount', tooltip : 'The tip amount' },
  date   : { content : 'Date', tooltip : 'The date that the tip was given' },
};

const colOrder = [ 'dealer', 'player', 'amount', 'date' ];

const rows = [{
  dealer : { content : 'Cherry' },
  player : { content : 'Crazy Bettor' },
  amount : { content : '$100' },
  date   : { content : '7/17/2015' },
}];

export default class Tips extends Component {

  constructor (props) {
    super(props);
    this.state = { rowData : rows };
  }

  render () {
    return (
      <Grid>
        <Row>
          <Column><Finder /></Column>
        </Row>

        <Row>
          <Column>
            <Table
              headerColumns={cols}
              columnOrder={colOrder}
              rowData={this.state.rowData}
              displaySelectAll={false}
              displayRowCheckbox={false} />
          </Column>
        </Row>
      </Grid>
    );
  }
}
