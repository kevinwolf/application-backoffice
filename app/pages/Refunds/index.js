import React, { Component } from 'react';
import Finder from '../../components/Finder';
import { Grid, Row, Column } from '../../components/Layout';
import { List, ListItem, Paper, DropDownMenu, TextField, RaisedButton } from 'material-ui';

const playersInformation = {
  age      : 25,
  country  : 'Costa Rica',
  currency : 'Colones',
  language : 'spanish',
  license  : 'Provider',
  name     : 'Marcelo',
  nickName : 'Maketroli',
  hands    : [{
    id        : 170794,
    date      : '7/17/1994',
    original  : 40,
    winner    : 100,
    blackjack : 135,
  }],
};

const refundTypes = [
  { payload : 'winner', text : 'Winner' },
  { payload : 'original', text : 'Original bet' },
  { payload : 'blackjack', text : 'Black Jack' },
  { payload : 'roulette', text : 'Roulette' },
];

export default class Refunds extends Component {

  render () {
    const hands = playersInformation.hands.map(hand => {
      return (
        <ListItem primaryText={`Hand id - ${hand.id}`} key={hand.id}>
          <ListItem primaryText={`Date - ${hand.date}`} />
          <ListItem primaryText={`Original bet amount - ${hand.original}`} />
          <ListItem primaryText={`Winner amount - ${hand.winner}`} />
          <ListItem primaryText={`Black Jack winner - ${hand.blackjack}`} />
        </ListItem>
      );
    });

    return (
      <Grid>
        <Row>
          <Column><Finder /></Column>
        </Row>

        <Row vertical-center>
          <Column>Player <strong>{`${playersInformation.name}`}</strong></Column>
          <Column text-right>
            <RaisedButton label="Request Refund" />
            <RaisedButton label="Refund History" />
          </Column>
        </Row>

        <Row>
          <Column>
            <Paper>
              <List subheader="Hands list">{hands}</List>
            </Paper>
          </Column>

          <Column>
            <Paper>
              <List subheader="Refund request">
                <ListItem>
                  <DropDownMenu menuItems={refundTypes} style={{ width : '100%' }} />
                </ListItem>
                <ListItem>
                  <TextField floatingLabelText="Refund amount" fullWidth={true} />
                </ListItem>
                <ListItem>
                  <TextField floatingLabelText="Details" multiLine={true} fullWidth={true} />
                </ListItem>
              </List>
            </Paper>
          </Column>
        </Row>
      </Grid>
    );
  }
}
