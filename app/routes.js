import React from 'react';
import { Route, DefaultRoute } from 'react-router';

// Layouts.
import Root from './layouts/Root';
import App from './layouts/App';

// Pages.
import Login from './pages/Login';
import Home from './pages/Home';
import About from './pages/About';
import PlayersInfo from './pages/PlayersInfo';
import Money from './pages/Money';
import Refunds from './pages/Refunds';
import Videos from './pages/Videos';
import Tips from './pages/Tips';

// Routes definition.
const Routes = (
  <Route name="root" path="/" handler={Root}>

    <Route name="app" handler={App}>
      <Route name="home" handler={Home} />
      <Route name="players-info" handler={PlayersInfo} />
      <Route name="money" handler={Money} />
      <Route name="refunds" handler={Refunds} />
      <Route name="videos" handler={Videos} />
      <Route name="tips" handler={Tips} />
      <Route name="about" handler={About} />
      <DefaultRoute handler={Home} />
    </Route>

    <Route name="login" handler={Login} />
    <DefaultRoute handler={Login} />
  </Route>
);

export default Routes;
