import React from 'react';
import { LeftNav } from 'material-ui';

export default class LeftNavMenu extends React.Component {

  static propTypes = {
    getActivePage : React.PropTypes.func,
    menuItems     : React.PropTypes.arrayOf(React.PropTypes.object),
  }

  static contextTypes = {
    router : React.PropTypes.func,
  }

  render () {
    return (
      <LeftNav
        ref="appNavbar"
        docked={false}
        menuItems={this.props.menuItems}
        selectedIndex={this.props.getActivePage()}
        onChange={this._onLeftNavChange} />
    );
  }

  // Navigate to route when clicking on a Side Bar element.
  _onLeftNavChange = (e, key, payload) => {
    this.context.router.transitionTo(payload.route);
  }
}
