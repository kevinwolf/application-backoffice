import React from 'react';
import { Tabs, Tab } from 'material-ui';

export default class TabsMainMenu extends React.Component {

  static propTypes = {
    getActivePage : React.PropTypes.func,
    menuItems     : React.PropTypes.arrayOf(React.PropTypes.object),
  }

  static contextTypes = {
    router : React.PropTypes.func,
  }

  render () {
    const tabs = this.props.menuItems.map((item) => {
      return (
        <Tab
          key={item.route}
          label={item.text}
          route={item.route}
          onActive={this._onActive} />
      );
    });

    return <Tabs initialSelectedIndex={this.props.getActivePage()}>{tabs}</Tabs>;
  }

  _onActive = tab => {
    this.context.router.transitionTo(tab.props.route);
  }

}
