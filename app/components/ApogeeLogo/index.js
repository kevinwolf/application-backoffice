import React, { Component } from 'react';
import apogeeLogo from './logo.png';

export default class ApogeeLogo extends Component {

  render () {
    return <image src={apogeeLogo} />;
  }

}
