import React from 'react';
import { TextField } from 'material-ui';

export default class Finder extends React.Component {

  static contextTypes = {
    router : React.PropTypes.func,
  }

  render () {
    return <TextField floatingLabelText="Search by username" />;
  }

}
