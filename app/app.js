import React from 'react';
import Router from 'react-router';
import Routes from './routes';

Router
  .create({
    routes         : Routes,
    scrollBehavior : Router.ScrollToTopBehavior,
  })
  .run((Handler) => React.render(<Handler />, document.body));
