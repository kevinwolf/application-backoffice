import React from 'react';
import { RouteHandler } from 'react-router';
import { Styles } from 'material-ui';
import injectTapEventPlugin from 'react-tap-event-plugin';
require('./style.scss');

const ThemeManager = new Styles.ThemeManager();

injectTapEventPlugin();

export default class App extends React.Component {

  static childContextTypes = {
    muiTheme : React.PropTypes.object,
  }

  getChildContext () {
    return {
      muiTheme : ThemeManager.getCurrentTheme(),
    };
  }

  render () {
    return <RouteHandler />;
  }

}
