import React from 'react';
import { RouteHandler } from 'react-router';
import ApogeeLogo from '../../components/ApogeeLogo';
import TabsMainMenu from '../../components/TabsMainMenu';
import LeftNavMenu from '../../components/LeftNavMenu';
import { AppBar, FlatButton } from 'material-ui';
require('./style.scss');

const menuItems = [
  { route : 'home', text : 'Game Info' },
  { route : 'players-info', text : 'Players Info' },
  { route : 'money', text : 'Money' },
  { route : 'refunds', text : 'Refunds' },
  { route : 'videos', text : 'Videos' },
  { route : 'tips', text : 'Tips' },
];

export default class App extends React.Component {

  static contextTypes = {
    router : React.PropTypes.func,
  }

  render () {
    return (
      <div>
        <AppBar
          title={<ApogeeLogo />}
          iconElementRight={<FlatButton label="Log out" onClick={this._logOut} />}
          onLeftIconButtonTouchTap={this._onLeftIconButtonTouchTap}
          zDepth={0} />

        <LeftNavMenu ref="appNavbar" menuItems={menuItems} getActivePage={this._getActivePage} />

        <TabsMainMenu menuItems={menuItems} getActivePage={this._getActivePage} />

        <RouteHandler />
      </div>
    );
  }

  // Toggle Side Bar.
  _onLeftIconButtonTouchTap = () => {
    this.refs.appNavbar.refs.appNavbar.toggle();
  }

  // Get the active page.
  _getActivePage = () => {
    for (const i in menuItems) {
      if (this.context.router.isActive(menuItems[i].route)) return parseInt(i, 10);
    }
  }

  // Send to log in screen.
  _logOut = () => {
    this.context.router.transitionTo('login');
  }

}
