'use strict';

var path         = require('path')
  , webpack      = require('webpack')
  , node_modules = path.resolve(__dirname, 'node_modules')
  , config;

config = {
  entry     : {
    app    : [ 'webpack-dev-server/client?http://0.0.0.0:8080', 'webpack/hot/only-dev-server', './app/app.js' ],
    vendor : [ 'react', 'react-router', 'react-tap-event-plugin', 'material-ui', 'classnames' ]
  },
  output    : {
    path     : path.resolve(__dirname, 'build'),
    filename : 'bundle.js'
  },
  module    : {
    loaders : [
      { test : /\.js$/, loader : 'react-hot!babel?optional[]=es7.classProperties', exclude : /node_modules/ },
      { test : /\.scss$/, loader : 'style!css!sass' },
      { test : /\.(png|jpg)$/, loader : 'url?limit=25000' }
    ]
  },
  plugins   : [
    new webpack.optimize.CommonsChunkPlugin('vendor', 'vendor.bundle.js')
  ]
};

module.exports = config;
